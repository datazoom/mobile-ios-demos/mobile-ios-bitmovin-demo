//
//  DZBitmovinCollector.h
//  DZBitmovinCollector
//
//  Created by Vishnu M P on 31/01/19.
//  Copyright © 2019 Data Zoom. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for DZBitmovinCollector.
FOUNDATION_EXPORT double DZBitmovinCollectorVersionNumber;

//! Project version string for DZBitmovinCollector.
FOUNDATION_EXPORT const unsigned char DZBitmovinCollectorVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DZBitmovinCollector/PublicHeader.h>
#import <DZBitmovinCollector/DZBitmovinCollector.h>

