//
//  PlayerViewController.swift
//  BitmovinDemo
//
//  Created by MAC on 05/08/18.
//  Copyright © 2018 MAC. All rights reserved.
//

import UIKit
import BitmovinPlayer
import DZBitmovinCollector

class PlayerViewController: UIViewController {
    
    var player              : BitmovinPlayer?
    var configId            :String!
    var connectionURL       :String!
    var buttonPushMe        : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        self.setUpPushMeButton()
    }
    
    deinit {
        player?.destroy()
    }
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
        // Define needed resources
        guard let streamUrl = URL(string:globalAssetValues) else {
            print("Please specify the needed resources marked with TODO in ViewController.swift file.")
            return
        }
        
        // Create player configuration
        let config = PlayerConfiguration()
        
        do {
            try config.setSourceItem(url: streamUrl)
            
            // Create player based on player configuration
            let player = BitmovinPlayer(configuration: config)
            
            // Create player view and pass the player instance to it
            let playerView = BMPBitmovinPlayerView(player: player, frame: .zero)
            
            // Listen to player events
            //player.add(listener: self as! PlayerListener)  // To be added on native connector
            
            playerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            playerView.frame = view.bounds
            
            view.addSubview(playerView)
            view.bringSubview(toFront: playerView)
            
            self.player = player
            
            DZBitmovinCollector.dzSharedManager.flagForAutomationOnly = false
            DZBitmovinCollector.dzSharedManager.initBitmovinPlayerWith(configID: configId, url: connectionURL, playerInstance: player)
            
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                //MARK:- Custom Evnets And Metadata
                let DZURL = NSURL(string: self.connectionURL)
                let domain = (DZURL?.host)!
                DZBitmovinCollector.dzSharedManager.customMetaData(["customPlayerName" : "iOS Native Player","customDomain":domain])
                DZBitmovinCollector.dzSharedManager.customEvnets("SDKLoaded", metadata: nil)
                //MARK:- Custom Evnets
                //DZNativeCollector.dzSharedManager.customEvnets("Dragged", metadata: nil)
                
                //MARK:- Custom Evnets And Metadata
                //DZNativeCollector.dzSharedManager.customEvnets("Dragged", metadata: ["Location" : "Starting"])
                
                //MARK:- Custom Metadata
                //DZNativeCollector.dzSharedManager.customEvnets(nil, metadata: ["Location" : "Ending"])
                
                //DZNativeCollector.dzSharedManager.customMetaData(["company" : "Adhoc","Location":"Technopark"])
                
            }
        }catch {
            print("Configuration error: \(error)")
        }
        //Dispose of any resources that can be recreated
    }
    
    func setUpPushMeButton() {
        self.buttonPushMe = UIButton(frame: CGRect(x: 0, y: 0, width: 90, height: 40))
        self.buttonPushMe.layer.cornerRadius = 10.0
        self.buttonPushMe.setTitle("Push Me", for: .normal)
        self.buttonPushMe.setTitleColor(UIColor.white, for: .normal)
        self.buttonPushMe.backgroundColor = UIColor(red: 34/255, green: 218/255, blue: 34/255, alpha: 1.0)
        self.buttonPushMe.addTarget(self, action: #selector(self.buttonPushTouched), for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: self.buttonPushMe)
        self.navigationItem.rightBarButtonItem = barButton
        //        self.view.addSubview(self.buttonPushMe)
        //        self.view.bringSubviewToFront(self.buttonPushMe)
    }
    
    
    @objc func buttonPushTouched() {
        DZBitmovinCollector.dzSharedManager.customEvnets("buttonPush", metadata: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}



